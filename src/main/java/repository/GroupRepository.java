package repository;

import java.sql.*;

public class GroupRepository {
    String url = "jdbc:postgresql://localhost:5432/task";
    String user = "postgres";
    String password = "123456";
    Connection connection = null;

    public void printGroups(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT  * From groupTable");
            System.out.println("List of groups");
            System.out.println("id| name " );
            while(resultSet.next()){
                System.out.println(resultSet.getInt("groupId") + " | " + resultSet.getString("name"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection != null){
                try{
                    connection.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }

}
