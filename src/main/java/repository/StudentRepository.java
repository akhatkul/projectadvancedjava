package repository;

import java.sql.*;

public class StudentRepository {
    String url = "jdbc:postgresql://localhost:5432/task";
    String user = "postgres";
    String password = "123456";
    Connection connection = null;

    public void printStudents(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT  * From studentTable");
            System.out.println("List of Students with their data");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("studentId") + " | " + resultSet.getString("fname")+ " | " + resultSet.getLong("lname")+ " | " + resultSet.getInt("groupId"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection != null){
                try{
                    connection.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }

}
