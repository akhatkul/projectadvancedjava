import repository.GroupRepository;
import repository.StudentRepository;

import java.sql.*;

public class Main {
    public static void main(String[] args)  {
        GroupRepository groupRep = new GroupRepository();
        StudentRepository studentRep = new StudentRepository();
        studentRep.printStudents();
        groupRep.printGroups();
    }

}
