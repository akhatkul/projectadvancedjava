CREATE DATABASE postgres;
CREATE TABLE groupTable(
                         groupId INT PRIMARY KEY,
                         name VARCHAR(255)
);
CREATE TABLE studentTable(
                           studentId INT PRIMARY KEY,
                           fname VARCHAR(255),
                           lname VARCHAR(255),
                           groupId INT,
                           FOREIGN KEY(groupId) REFERENCES groupTable(groupId)
);
